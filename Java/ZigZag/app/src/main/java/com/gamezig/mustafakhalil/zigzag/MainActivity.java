package com.gamezig.mustafakhalil.zigzag;

import android.graphics.Rect;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import static com.gamezig.mustafakhalil.zigzag.MovementView.sendViewToBack;

/**
 *
 *  ZigZag Game
 *  Created by
 *   Name: Ebru Emirli    ID:  120201010
 *   Name: Büşra Oğuz     ID:  130201018
 *   Name: Mustafa Khalil ID:  140201100
 *
 */

public class MainActivity extends AppCompatActivity {

    final int delay = 45;
    CheckIfOnPillar rectChecker = new CheckIfOnPillar();
    boolean gameEnd, ballMovement;
    int scoreValue, highScoreValue;
    RelativeLayout gameLayout;
    Button playButton, retryButton;
    TextView scoreOnboard, highScoreBoard, score;
    ImageView ball, gameOver, scoreBoard, startGameImage;
    ImageView pillar1,pillar2,pillar3,pillar4,pillar5,pillar6,pillar7,pillar8,pillar9,pillar10,pillar11,pillar12;
    ImageView pillar13,pillar14,pillar15,pillar16,pillar17,pillar18,pillar19,pillar20;
    ImageView pillarTop1, pillarTop2, pillarTop3, pillarTop4, pillarTop5, pillarTop6;
    MovementView move = new MovementView();
    private final android.os.Handler handler = new android.os.Handler();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        onCreateNew();
        playButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View click){
                playClicked();
                movePillar();
            }
        });
    }

    /**
     *    Method onCreateNew
     *   
     *    The method links the images with their parameters 
     *    and calls another method to make them invisible until the game starts
     * 
     */

    protected void onCreateNew(){

        startGameImage = ball = (ImageView)findViewById(R.id.zig);
        gameLayout = (RelativeLayout)findViewById(R.id.activity_main);
        playButton = (Button)findViewById(R.id.playBtn);
        retryButton = (Button)findViewById(R.id.retryBtn);
        score = (TextView)findViewById(R.id.score);
        highScoreBoard = (TextView)findViewById(R.id.highscoreOnBoard);
        scoreOnboard = (TextView)findViewById(R.id.scoreOnBoard);
        ball = (ImageView)findViewById(R.id.ball);
        gameOver = (ImageView)findViewById(R.id.gameOver);
        scoreBoard = (ImageView)findViewById(R.id.scoreBoard);
        pillar1 = (ImageView)findViewById(R.id.Pillar1);
        pillar2 = (ImageView)findViewById(R.id.Pillar2);
        pillar3 = (ImageView)findViewById(R.id.Pillar3);
        pillar4 = (ImageView)findViewById(R.id.Pillar4);
        pillar5 = (ImageView)findViewById(R.id.Pillar5);
        pillar6 = (ImageView)findViewById(R.id.Pillar6);
        pillar7 = (ImageView)findViewById(R.id.Pillar7);
        pillar8 = (ImageView)findViewById(R.id.Pillar8);
        pillar9 = (ImageView)findViewById(R.id.Pillar9);
        pillar10 = (ImageView)findViewById(R.id.Pillar10);
        pillar11 = (ImageView)findViewById(R.id.Pillar11);
        pillar12 = (ImageView)findViewById(R.id.Pillar12);
        pillar13 = (ImageView)findViewById(R.id.Pillar13);
        pillar14 = (ImageView)findViewById(R.id.Pillar14);
        pillar15 = (ImageView)findViewById(R.id.Pillar15);
        pillar16 = (ImageView)findViewById(R.id.Pillar16);
        pillar17 = (ImageView)findViewById(R.id.Pillar17);
        pillar18 = (ImageView)findViewById(R.id.Pillar18);
        pillar19 = (ImageView)findViewById(R.id.Pillar19);
        pillar20 = (ImageView)findViewById(R.id.Pillar20);

        pillarTop1 = (ImageView)findViewById(R.id.PillarTop1);
        pillarTop2 = (ImageView)findViewById(R.id.PillarTop2);
        pillarTop3 = (ImageView)findViewById(R.id.PillarTop3);
        pillarTop4 = (ImageView)findViewById(R.id.PillarTop4);
        pillarTop5 = (ImageView)findViewById(R.id.PillarTop5);
        pillarTop6 = (ImageView)findViewById(R.id.PillarTop6);

        gameEnd = false;
        ballMovement = true;
        scoreValue = 0;
        cantBeSeen();

    }

    /**
     *   Method assignRectToImage
     *   Links the Images with their rects to keep tracking them while the game is started
     *   by assigning them to the pillars, pillartops, ball from the CheckIfOnPillar class
     */
    
    protected void assignRectToImage(){
        ball.getHitRect(rectChecker.ballRect);
        pillar1.getHitRect(rectChecker.pillar1);
        pillar2.getHitRect(rectChecker.pillar2);
        pillar3.getHitRect(rectChecker.pillar3);
        pillar4.getHitRect(rectChecker.pillar4);
        pillar5.getHitRect(rectChecker.pillar5);
        pillar6.getHitRect(rectChecker.pillar6);
        pillar7.getHitRect(rectChecker.pillar7);
        pillar8.getHitRect(rectChecker.pillar8);
        pillar9.getHitRect(rectChecker.pillar9);
        pillar10.getHitRect(rectChecker.pillar10);
        pillar11.getHitRect(rectChecker.pillar11);
        pillar12.getHitRect(rectChecker.pillar12);
        pillar13.getHitRect(rectChecker.pillar13);
        pillar14.getHitRect(rectChecker.pillar14);
        pillar15.getHitRect(rectChecker.pillar15);
        pillar16.getHitRect(rectChecker.pillar16);
        pillar17.getHitRect(rectChecker.pillar17);
        pillar18.getHitRect(rectChecker.pillar18);
        pillar19.getHitRect(rectChecker.pillar19);
        pillar20.getHitRect(rectChecker.pillar20);
        pillarTop1.getHitRect(rectChecker.pillarTop1);
        pillarTop2.getHitRect(rectChecker.pillarTop2);
        pillarTop3.getHitRect(rectChecker.pillarTop3);
        pillarTop4.getHitRect(rectChecker.pillarTop4);
        pillarTop5.getHitRect(rectChecker.pillarTop5);
        pillarTop6.getHitRect(rectChecker.pillarTop6);
    }

    /**
     *   Method SetXYPillarTop
     *   @param pil array of pillars
     *   @param piltop array of pillartops
     *
     *   The pillartops are going to take the position of the pillars so the ball rect would hit
     *                  the hit box of the pillarTop
     */
    
    protected void setXYPillarTop(ImageView[] piltop, ImageView[] pil){
        for (int i = 0; i < piltop.length; i++){
            piltop[i].setX(pil[i].getX()+ 10.5f);
            piltop[i].setY(pil[i].getY()+ 10.5f);
        }
    }

    /**
     *   Method OutOfscreen
     *   @param pil pillar that's out of screen
     *   @param pil2 pillar that's at the top of the stack
     *
     *   1- sends the image at the end of the XML stack
     *   2- it sets the pillar that's out of screen with the coordinates of the one on the top of the stack, by
     *               calling the move parameter that corresponds with the MovementView Class
     *
     */
    protected void outOfscreen(ImageView pil,ImageView pil2){
        if (move.checkPillarPosition(pil.getY())==true)
        {
            // sendViewToBack is in the MovementView class

            sendViewToBack(pil);

            // move.pillarPlacement is a method is class MovementView

            pil.setX(move.pillarPlacementX(pil2.getX()));
            pil.setY(move.pillarPlacementY(pil2.getY()));
        }
    }

    /**
     * Method goingDown
     * @param pil array of pillars
     *
     *            the main idea of this method is making the pillars go down so we can achieve the
     *            movement effect
     */

    protected void goingDown(ImageView [] pil){
        for(int i = 0; i < pil.length;i++){
            pil[i].setY(pil[i].getY() + 5);
        }

    }

    /**
     * method setINVISIBLE
     * @param pil array of pillars
     *            make them invisible
     */

    protected void setINVISIBLE(ImageView [] pil){
        for (int i = 0; i<pil.length; i++){
            pil[i].setVisibility(View.INVISIBLE);
        }
    }

    /**
     * method setVISIBLE
     * @param pil array of pillars
     *            make them visible
     */

    protected void setVISIBLE(ImageView [] pil){
        for (int i = 0; i<pil.length; i++){
            pil[i].setVisibility(View.VISIBLE);
        }
    }

    /**
     * method setXYStartGame
     * @param pil array of pillars
     *
     *            sets the coordinates of X and Y according to the pillars before.
     *            it starts from 3 cause the array has all the items "Pillar1,Pillar2,......Pillar20"
     *            and number i=3 is the pillar number four; the one that hasn't been assigned to X and Y yet
     */

    protected void setXYStartGame(ImageView [] pil){
        for(int i = 3; i < pil.length;i++){
            pil[i].setX(move.pillarPlacementX(pil[i-1].getX()));
            pil[i].setY(move.pillarPlacementY(pil[i-1].getY()));
        }

    }

    /**
     * Method cantBeSeen
     * makes everything Invisible by their setting them to invisible manually or by calling other methods
     */


    private void cantBeSeen(){
        ImageView ImagesrArray[] = {pillar1,pillar2,pillar3,pillar4,pillar5,pillar6,pillar7,pillar8,pillar9,pillar10,
                pillar11,pillar12,pillar13,pillar14,pillar15,pillar16,pillar17,pillar18,pillar19,pillar20,
                pillarTop1,pillarTop2,pillarTop3,pillarTop4,pillarTop5,pillarTop6,ball,gameOver,scoreBoard};
        setINVISIBLE(ImagesrArray);
        retryButton.setVisibility(View.INVISIBLE);
        score.setVisibility(View.INVISIBLE);
        highScoreBoard.setVisibility(View.INVISIBLE);
        scoreOnboard.setVisibility(View.INVISIBLE);

    }

    /**
     * method playClicked
     *
     *  Sets the start screen as invisible
     *  sets the starting point of the ball, Pillar1, pillar2, Pillar3; then calls setXYStartGame()
     *  sets the ball,pillars visible
     */

    protected void playClicked(){

        playButton.setVisibility(View.INVISIBLE);
        startGameImage.setVisibility(View.INVISIBLE);

        pillar1.setX(320);
        pillar1.setY(768);
        ball.setX(330);
        ball.setY(820);
        pillar2.setX(pillar1.getX()+ 78);
        pillar2.setY(pillar1.getY() - 55);
        pillar3.setX(pillar2.getX()+ 78);
        pillar3.setY(pillar2.getY() - 55);
        pillarTop1.setX(pillar2.getX()+10.5f);
        pillarTop1.setY(pillar2.getY()+10.5f);
        pillarTop2.setX(pillar1.getX()+10.5f);
        pillarTop2.setY(pillar1.getY()+10.5f);

        ImageView PillarArray[] = {pillar1,pillar2,pillar3,pillar4,pillar5,pillar6,pillar7,pillar8,pillar9,pillar10,
                pillar11,pillar12,pillar13,pillar14,pillar15,pillar16,pillar17,pillar18,pillar19,pillar20};
        setXYStartGame(PillarArray);
        setVISIBLE(PillarArray);

        ImageView PillarTopArray[] = {pillarTop1,pillarTop2,pillarTop3,pillarTop4};
        setVISIBLE(PillarTopArray);

        score.setVisibility(View.VISIBLE);
        ball.setVisibility(View.VISIBLE);

    }

    /**
     * method movePillar
     *  refreshes the screen every 45 mill sec.
     *  moves the ball right and left.
     *  makes the pillars go down
     *  calls assignRectToImage() that assigns the rects to their images
     *  outofscreen() is called that reposition the pillars
     *  checks if the rects intersects 1- Yes, the game continues 2- No, gameOver is called
     */
    protected void movePillar(){

        handler.postDelayed(new Runnable() {
            @Override
            public void run() {

                gameLayout.setOnTouchListener(new View.OnTouchListener() {
                    @Override
                    public boolean onTouch(View view, MotionEvent motionEvent) {

                        if (motionEvent.getAction() == MotionEvent.ACTION_UP){
                            if (gameEnd == false){
                                scoreValue++;
                                score.setText(String.valueOf(scoreValue));
                                if (ballMovement == true){
                                    ballMovement = false;
                                }
                                else{
                                    ballMovement = true;
                                }
                            }
                            return true;

                        }
                        else{
                            return false;
                        }
                    }
                });

                if (ballMovement == true){
                    ball.setX(ball.getX() + 6.7f);
                    ball.setY(ball.getY() - 0.5f);
                }
                else{
                    ball.setX(ball.getX() - 6.7f);
                    ball.setY(ball.getY() - 0.5f);
                }
                ImageView PillarArray[] = {pillar1,pillar2,pillar3,pillar4,pillar5,pillar6,pillar7,pillar8,pillar9,pillar10,
                        pillar11,pillar12,pillar13,pillar14,pillar15,pillar16,pillar17,pillar18,pillar19,pillar20};

                goingDown(PillarArray);
                assignRectToImage();

                ball.setY(ball.getY() + 0.5f);

                outOfscreen(pillar1,pillar20);
                outOfscreen(pillar2,pillar1);
                outOfscreen(pillar3,pillar2);
                outOfscreen(pillar4,pillar3);
                outOfscreen(pillar5,pillar4);
                outOfscreen(pillar6,pillar5);
                outOfscreen(pillar7,pillar6);
                outOfscreen(pillar8,pillar7);
                outOfscreen(pillar9,pillar8);
                outOfscreen(pillar10,pillar9);
                outOfscreen(pillar11,pillar10);
                outOfscreen(pillar12,pillar11);
                outOfscreen(pillar13,pillar12);
                outOfscreen(pillar14,pillar13);
                outOfscreen(pillar15,pillar14);
                outOfscreen(pillar16,pillar15);
                outOfscreen(pillar17,pillar16);
                outOfscreen(pillar18,pillar17);
                outOfscreen(pillar19,pillar18);
                outOfscreen(pillar20,pillar19);

                if (rectChecker.checkRect() == true)
                {

                }
                else{

                    ball.setVisibility(View.INVISIBLE);
                    ball.setX(330);
                    ball.setY(820);
                    gameOver();
                }
                checkIfIntersects();
                handler.postDelayed(this,delay);
            }
        }, delay);

    }

    /**
     * Method checkIfIntersects
     *
     * check if the pillar rects and the ball rect intersects, and reposition them
     * by calling setXYPillarTop()
     *
     */

    public void checkIfIntersects(){
        if (Rect.intersects(rectChecker.ballRect,rectChecker.pillar1)){

            pillarTop4.setVisibility(View.VISIBLE);
            pillarTop5.setVisibility(View.VISIBLE);
            pillarTop6.setVisibility(View.VISIBLE);
            ImageView [] pillarTopArray = {pillarTop1,pillarTop2,pillarTop3,pillarTop4,pillarTop5,pillarTop6};
            ImageView [] pillarArray = {pillar2,pillar1,pillar20,pillar19,pillar18,pillar17};
            setXYPillarTop(pillarTopArray,pillarArray);

        }
        else if (Rect.intersects(rectChecker.ballRect,rectChecker.pillar2))
        {

            ImageView [] pillarTopArray = {pillarTop1,pillarTop2,pillarTop3,pillarTop4,pillarTop5,pillarTop6};
            ImageView [] pillarArray = {pillar3,pillar2,pillar1,pillar20,pillar19,pillar18};
            setXYPillarTop(pillarTopArray,pillarArray);

        }
        else if (Rect.intersects(rectChecker.ballRect,rectChecker.pillar3))
        {
            pillarTop5.setVisibility(View.INVISIBLE);
            pillarTop6.setVisibility(View.INVISIBLE);
            ImageView [] pillarTopArray = {pillarTop1,pillarTop2,pillarTop3,pillarTop4};
            ImageView [] pillarArray = {pillar4,pillar3,pillar2,pillar1};
            setXYPillarTop(pillarTopArray,pillarArray);

        }
        else if (Rect.intersects(rectChecker.ballRect,rectChecker.pillar4))
        {
            ImageView [] pillarTopArray = {pillarTop1,pillarTop2,pillarTop3,pillarTop4};
            ImageView [] pillarArray = {pillar6,pillar5,pillar4,pillar3};
            setXYPillarTop(pillarTopArray,pillarArray);
        }
        else if (Rect.intersects(rectChecker.ballRect,rectChecker.pillar5))
        {
            ImageView [] pillarTopArray = {pillarTop1,pillarTop2,pillarTop3,pillarTop4};
            ImageView [] pillarArray = {pillar7,pillar6,pillar5,pillar4};
            setXYPillarTop(pillarTopArray,pillarArray);

        }
        else if (Rect.intersects(rectChecker.ballRect,rectChecker.pillar6))
        {
            ImageView [] pillarTopArray = {pillarTop1,pillarTop2,pillarTop3,pillarTop4};
            ImageView [] pillarArray = {pillar8,pillar7,pillar6,pillar5};
            setXYPillarTop(pillarTopArray,pillarArray);

        }
        else if (Rect.intersects(rectChecker.ballRect,rectChecker.pillar7))
        {
            ImageView [] pillarTopArray = {pillarTop1,pillarTop2,pillarTop3,pillarTop4};
            ImageView [] pillarArray = {pillar9,pillar8,pillar7,pillar6};
            setXYPillarTop(pillarTopArray,pillarArray);

        }
        else if (Rect.intersects(rectChecker.ballRect,rectChecker.pillar8))
        {
            ImageView [] pillarTopArray = {pillarTop1,pillarTop2,pillarTop3,pillarTop4};
            ImageView [] pillarArray = {pillar10,pillar9,pillar8,pillar7};
            setXYPillarTop(pillarTopArray,pillarArray);

        }
        else if (Rect.intersects(rectChecker.ballRect,rectChecker.pillar9))
        {
            ImageView [] pillarTopArray = {pillarTop1,pillarTop2,pillarTop3,pillarTop4};
            ImageView [] pillarArray = {pillar11,pillar10,pillar9,pillar8};
            setXYPillarTop(pillarTopArray,pillarArray);

        }
        else if (Rect.intersects(rectChecker.ballRect,rectChecker.pillar10))
        {
            ImageView [] pillarTopArray = {pillarTop1,pillarTop2,pillarTop3,pillarTop4};
            ImageView [] pillarArray = {pillar12,pillar11,pillar10,pillar9};
            setXYPillarTop(pillarTopArray,pillarArray);

        }
        else if (Rect.intersects(rectChecker.ballRect,rectChecker.pillar11))
        {
            ImageView [] pillarTopArray = {pillarTop1,pillarTop2,pillarTop3,pillarTop4};
            ImageView [] pillarArray = {pillar13,pillar12,pillar11,pillar10};
            setXYPillarTop(pillarTopArray,pillarArray);

        }
        else if (Rect.intersects(rectChecker.ballRect,rectChecker.pillar12))
        {
            ImageView [] pillarTopArray = {pillarTop1,pillarTop2,pillarTop3,pillarTop4};
            ImageView [] pillarArray = {pillar14,pillar13,pillar12,pillar11};
            setXYPillarTop(pillarTopArray,pillarArray);

        }
        else if (Rect.intersects(rectChecker.ballRect,rectChecker.pillar13))
        {
            ImageView [] pillarTopArray = {pillarTop1,pillarTop2,pillarTop3,pillarTop4};
            ImageView [] pillarArray = {pillar15,pillar14,pillar13,pillar12};
            setXYPillarTop(pillarTopArray,pillarArray);

        }
        else if (Rect.intersects(rectChecker.ballRect,rectChecker.pillar14))
        {
            ImageView [] pillarTopArray = {pillarTop1,pillarTop2,pillarTop3,pillarTop4};
            ImageView [] pillarArray = {pillar16,pillar15,pillar14,pillar13};
            setXYPillarTop(pillarTopArray,pillarArray);
        }
        else if (Rect.intersects(rectChecker.ballRect,rectChecker.pillar15))
        {
            ImageView [] pillarTopArray = {pillarTop1,pillarTop2,pillarTop3,pillarTop4};
            ImageView [] pillarArray = {pillar17,pillar16,pillar15,pillar14};
            setXYPillarTop(pillarTopArray,pillarArray);

        }
        else if (Rect.intersects(rectChecker.ballRect,rectChecker.pillar16))
        {
            ImageView [] pillarTopArray = {pillarTop1,pillarTop2,pillarTop3,pillarTop4};
            ImageView [] pillarArray = {pillar18,pillar17,pillar16,pillar15};
            setXYPillarTop(pillarTopArray,pillarArray);

        }
        else if (Rect.intersects(rectChecker.ballRect,rectChecker.pillar17))
        {
            ImageView [] pillarTopArray = {pillarTop1,pillarTop2,pillarTop3,pillarTop4};
            ImageView [] pillarArray = {pillar19,pillar18,pillar17,pillar16};
            setXYPillarTop(pillarTopArray,pillarArray);
        }
        else if (Rect.intersects(rectChecker.ballRect,rectChecker.pillar18))
        {
            ImageView [] pillarTopArray = {pillarTop1,pillarTop2,pillarTop3,pillarTop4};
            ImageView [] pillarArray = {pillar20,pillar19,pillar18,pillar17};
            setXYPillarTop(pillarTopArray,pillarArray);
        }
        else if (Rect.intersects(rectChecker.ballRect,rectChecker.pillar19))
        {
            ImageView [] pillarTopArray = {pillarTop1,pillarTop2,pillarTop3,pillarTop4};
            ImageView [] pillarArray = {pillar1,pillar20,pillar19,pillar18};
            setXYPillarTop(pillarTopArray,pillarArray);


        }
        else if (Rect.intersects(rectChecker.ballRect,rectChecker.pillar20)) {
            ImageView [] pillarTopArray = {pillarTop1,pillarTop2,pillarTop3,pillarTop4};
            ImageView [] pillarArray = {pillar2,pillar1,pillar20,pillar19};
            setXYPillarTop(pillarTopArray,pillarArray);
        }
    }

    /**
     * Method gameOver()
     *  makes the game end
     *  hides the pillars, ball. shows the end screen with a retry button
     */
    public void gameOver(){
        gameEnd = true;
        handler.removeCallbacksAndMessages(null);

        ImageView PillarArray[] = {pillar1,pillar2,pillar3,pillar4,pillar5,pillar6,pillar7,pillar8,pillar9,pillar10,
                pillar11,pillar12,pillar13,pillar14,pillar15,pillar16,pillar17,pillar18,pillar19,pillar20};
        for(int i = 0; i < PillarArray.length; i++){
            sendViewToBack(PillarArray[i]);
        }

        setINVISIBLE(PillarArray);

        ImageView PillartopArray[] = {pillarTop1,pillarTop2,pillarTop3,pillarTop4,pillarTop5,pillarTop6};
        setINVISIBLE(PillartopArray);

        ball.setVisibility(View.INVISIBLE);
        score.setVisibility(View.INVISIBLE);

        if (scoreValue > highScoreValue){
            highScoreValue = scoreValue;
            highScoreBoard.setText(String.valueOf(highScoreValue));
            
        }
        highScoreBoard.setText(String.valueOf(highScoreValue));
        scoreOnboard.setText(String.valueOf(scoreValue));
        retryButton.setVisibility(View.VISIBLE);
        highScoreBoard.setVisibility(View.VISIBLE);
        scoreOnboard.setVisibility(View.VISIBLE);
        scoreBoard.setVisibility(View.VISIBLE);
        gameOver.setVisibility(View.VISIBLE);

        retryButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                onCreateNew();
                playClicked();
                movePillar();
            }
        });
    }
}
