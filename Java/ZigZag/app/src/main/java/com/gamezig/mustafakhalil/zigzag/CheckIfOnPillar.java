package com.gamezig.mustafakhalil.zigzag;

import android.graphics.Rect;

/**
 *
 *  ZigZag Game
 *  Created by
 *   Name: Ebru Emirli    ID:  120201010
 *   Name: Büşra Oğuz     ID:  130201018
 *   Name: Mustafa Khalil ID:  140201100
 *
 */

public class CheckIfOnPillar{
    /**
     * Creating rects for the pillars, pillartops, and ball
     * checking if the pillar is on the top of the pillar tops
     * 
     */
    Rect ballRect = new Rect();
    Rect pillar1 = new Rect();
    Rect pillar2 = new Rect();
    Rect pillar3 = new Rect();
    Rect pillar4 = new Rect();
    Rect pillar5 = new Rect();
    Rect pillar6 = new Rect();
    Rect pillar7 = new Rect();
    Rect pillar8 = new Rect();
    Rect pillar9 = new Rect();
    Rect pillar10 = new Rect();
    Rect pillar11 = new Rect();
    Rect pillar12 = new Rect();
    Rect pillar13 = new Rect();
    Rect pillar14 = new Rect();
    Rect pillar15 = new Rect();
    Rect pillar16 = new Rect();
    Rect pillar17 = new Rect();
    Rect pillar18 = new Rect();
    Rect pillar19 = new Rect();
    Rect pillar20 = new Rect();
    Rect pillarTop1 = new Rect();
    Rect pillarTop2 = new Rect();
    Rect pillarTop3 = new Rect();
    Rect pillarTop4 = new Rect();
    Rect pillarTop5 = new Rect();
    Rect pillarTop6 = new Rect();

    public CheckIfOnPillar(){

    }

    protected boolean checkRect(){
        final int x_out = 50;
        final int y_out = 48;
        boolean ballOnRect = (ballRect.centerX() > pillarTop1.centerX() - x_out) &&
                (ballRect.centerX() < pillarTop1.centerX() + x_out) &&
                (ballRect.centerY() > pillarTop1.centerY() - y_out) &&
                (ballRect.centerY() < pillarTop1.centerY() + y_out) ||
                ((ballRect.centerX() > pillarTop2.centerX() - x_out) &&
                        (ballRect.centerX() < pillarTop2.centerX() + x_out) &&
                        (ballRect.centerY() > pillarTop2.centerY() - y_out) &&
                        (ballRect.centerY() < pillarTop2.centerY() + y_out)) ||
                ((ballRect.centerX() > pillarTop3.centerX() - x_out) &&
                        (ballRect.centerX() < pillarTop3.centerX() + x_out) &&
                        (ballRect.centerY() > pillarTop3.centerY() - y_out) &&
                        (ballRect.centerY() < pillarTop3.centerY() + y_out)) ||
                ((ballRect.centerX() < pillarTop4.centerX() - x_out) &&
                        (ballRect.centerX() > pillarTop4.centerX() + x_out) &&
                        (ballRect.centerY() < pillarTop4.centerY() - y_out) &&
                        (ballRect.centerY() > pillarTop4.centerY() + y_out)) ||
                ((ballRect.centerX() < pillarTop5.centerX() - x_out) &&
                        (ballRect.centerX() > pillarTop5.centerX() + x_out) &&
                        (ballRect.centerY() < pillarTop5.centerY() - y_out) &&
                        (ballRect.centerY() > pillarTop5.centerY() + y_out)) ||
                ((ballRect.centerX() < pillarTop6.centerX() - x_out) &&
                        (ballRect.centerX() > pillarTop6.centerX() + x_out) &&
                        (ballRect.centerY() < pillarTop6.centerY() - y_out) &&
                        (ballRect.centerY() > pillarTop6.centerY() + y_out));
        return ballOnRect;
    }

}
