package com.gamezig.mustafakhalil.zigzag;


import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewGroup;

/**
 *
 *  ZigZag Game
 *  Created by
 *   Name: Ebru Emirli    ID:  120201010
 *   Name: Büşra Oğuz     ID:  130201018
 *   Name: Mustafa Khalil ID:  140201100
 *
 */

public class MovementView extends AppCompatActivity {

    public MovementView(){

    }

    /**
     * Method pillarPlacementX
     * @param x the value of the first position of the old pillar
     *
     * @return the new position of the pillar on the x axis according to the old pillar
     *
     */
    public float pillarPlacementX(float x){
        float pillerNewX;
        int random = (int) (Math.random()* 2 + 1);
        if (random == 1){
            if (x>600){
                pillerNewX = x - 79;
            }
            else {
                pillerNewX = x + 78;
            }
        }
        else{
            if (x <40){
                pillerNewX = x + 78;
            }
            else {

                pillerNewX = x - 79;
            }
        }
        return pillerNewX;
    }

    /**
     * Method pillarPlacementY
     * @param y the value of the first position of the old pillar
     *
     * @return the new position of the pillar on the y axis according to the old pillar
     */

    public float pillarPlacementY(float y){
        float pillarNewY = y - 57;
        return pillarNewY;
    }

    /**
     * Method checkPillarPosition
     * @param offScreen checks if the pillar is offscreen
     *
     * @return boolean that tells if the pillars if offscreen
     */
    public boolean checkPillarPosition(float offScreen){
        boolean low = false;
        if (offScreen > 1000f){
            low = true;
        }
        return low;
    }

    /**
     * method sendViewToBack
     * @param child Pillar
     *              sends the pillar to the back of the stack of the XML file
     */

    public static void sendViewToBack(final View child){
        final ViewGroup parent = (ViewGroup)child.getParent();
        if (null!=parent){
            parent.removeView(child);
            parent.addView(child,6);
        }
    }

}
