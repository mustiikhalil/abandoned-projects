/**
 * Created by mustafakhalil on 11/30/16.
 */

public class test {
    public static void main(String []args){
        Food x = new Food("chips",9.0,"lazy","chips");
        Beverage water = new Beverage("water",1.0,"aqua",false);
        Apparel mellon = new Apparel("waterMellon",0.2,"whatever","summer");
        Hardware computer = new Hardware("mac", 2000.0, "Apple",2);
        shoppingCart cart = new shoppingCart();
        cart.addItem(mellon);
        cart.addItem(water);
        cart.addItem(computer);
        cart.addItem(x);
        Beverage cola = new Beverage();
        cola.setName("Cola");
        cola.setBrand("Pepsi");
        cola.setCost(2.3);
        cart.addItem(cola);
        cola.setAlcoholic(false);
        System.out.println(cart.toString());
    }
}
