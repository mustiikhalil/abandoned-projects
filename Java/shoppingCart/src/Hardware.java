/**
 * Created by mustafakhalil on 11/30/16.
 */
public class Hardware extends items{
    private int warranty = 0;

    public Hardware(items newitems, int newWarranty){
        super(newitems);
        warranty = newWarranty;
    }

    public Hardware(){

    }

    public Hardware(String newName, Double newCost,String newbrand, int newWarranty){
        super(newName,newCost,newbrand);
        this.warranty = newWarranty;
    }


    public int getWarranty(){
        return this.warranty;
    }

    public void setWarranty(int newWarranty){
        this.warranty = newWarranty;
    }

    public String toString(){
        return getName() + "\t\t" + getCost() + "\t\t" + getBrand() + "\t\t" + warranty;
    }
}
