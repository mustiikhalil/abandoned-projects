import java.util.ArrayList;

/**
 * Created by mustafakhalil on 12/1/16.
 */

public class shoppingCart{

    private ArrayList<items> shopping = new ArrayList<items>();

    public shoppingCart(){

    }

    public void addItem(items name){
        shopping.add(name);
    }

    public String toString(){
        Double total = 0.0;
        String list = "----------------------------------\n";
        for (int i = 0; i < shopping.size(); i++) {
            list += shopping.get(i).toString()+ " \n";
            total += shopping.get(i).getCost();
        }
        list += "----------------------------------\n";
        list += "total = "+ total;
        return list;

    }

 }
