/**
 * Created by mustafakhalil on 11/30/16.
 */
public class Beverage extends items {
    private boolean isAlcoholic = false;

    public Beverage(items newitems, boolean newAlcoholic){
        super(newitems);
        isAlcoholic = newAlcoholic;
    }
    public Beverage(){

    }

    public Beverage(String newName, Double newCost,String newbrand,boolean Alcoholic){
        super(newName,newCost,newbrand);
        this.isAlcoholic = Alcoholic;
    }


    public boolean getAlcoholic(){
        return this.isAlcoholic;
    }

    public void setAlcoholic(boolean Alcoholic){
        this.isAlcoholic = Alcoholic;
    }

    public String toString(){
        return getName() + "\t\t" + getCost() + "\t\t" + getBrand() + "\t\t" + isAlcoholic;
    }
}
