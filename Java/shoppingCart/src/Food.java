/**
 * Created by mustafakhalil on 11/30/16.
 */
public class Food extends items{
    private String type = "";

    public Food(items newitems, String newType){
        super(newitems);
        type = newType;
    }
    public Food(){

    }

    public Food(String newName, Double newCost,String newbrand,String newtype){
        super(newName,newCost,newbrand);
        this.type = newtype;
    }


    public String getType(){
        return this.type;
    }

    public void setType(String newtype){
        this.type = newtype;
    }

    public String toString(){
        return getName() + "\t\t" + getCost() + "\t\t" + getBrand() + "\t\t" + type;
    }
}
