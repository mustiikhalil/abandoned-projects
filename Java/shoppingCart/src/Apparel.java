/**
 * Created by mustafakhalil on 11/30/16.
 */
public class Apparel extends items{
    private String season = "";

    public Apparel(items newitems, String newSeason){
        super(newitems);
        season = newSeason;
    }
    public Apparel(){

    }

    public Apparel(String newName, Double newCost,String newbrand,String newSeason){
        super(newName,newCost,newbrand);
        this.season = newSeason;
    }

    public String getSeason(){
        return this.season;
    }

    public void setSeason(String newSeason){
        this.season = newSeason;
    }

    public String toString(){
        return getName() + "\t\t" + getCost() + "\t\t" + getBrand() + "\t\t" + season;
    }
}
