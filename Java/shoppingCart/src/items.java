/**
 * Created by mustafakhalil on 11/30/16.
 */
public class items {
    private String name = "";
    private double cost = 0.0;
    private String brand = "";
    public  items(items newItem){
        cost = newItem.getCost();
        name = newItem.getName();
        brand = newItem.getBrand();
    }
    public items(){

    }
    public items(String newName, Double newCost,String newbrand){
        this.name = newName;
        this.brand = newbrand;
        this.cost = newCost;
    }

    public String getName(){
        return this.name;
    }

    public void setName(String newName){
        this.name = newName;
    }

    public String getBrand(){
        return this.brand;
    }

    public void setBrand(String newBrand){
        this.brand = newBrand;
    }

    public double getCost(){
        return this.cost;
    }

    public void setCost(Double newCost){
        this.cost = newCost;
    }

    public String toString(){
        return name + " " + cost + " " + brand + " ";
    }


}
