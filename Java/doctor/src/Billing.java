/**
 * Created by mustafakhalil on 12/1/16.
 */
public class Billing {
    private Patient patient;
    private Doctor doctor;
    public Billing(){

    }
    public Billing(Patient p, Doctor d){
        this.patient = p;
        this.doctor = d;
    }
    public Patient getPatient(){
        return this.patient;
    }
    public Doctor getDoctor(){
        return this.doctor;
    }
    public void setPatient(Patient pat){
        this.patient = pat;
    }
    public void setDoctor(Doctor doc){
        this.doctor = doc;
    }
    public String toString(){
        return "\nDoctor Info\n"+ doctor.toString() + "\n\nPatient Info\n" + patient.toString();
    }
    public void printBill(){
        System.out.println("\nDoctor Info\n"+ doctor.toString() + "\n\nPatient Info\n" + patient.toString());
    }
    public double getfee(){
        return doctor.getdoctorFee();
    }

}
