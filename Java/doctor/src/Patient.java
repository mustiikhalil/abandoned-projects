/**
 * Created by mustafakhalil on 12/1/16.
 */
public class Patient extends Person{
    private String identification;

    public Patient(){
        this.identification = "Patient Zero";
    }

    public Patient(String name, String id){
        super(name);
        this.identification = id;
    }

    public String getId(){
        return identification;
    }

    public void getId(String id){
        this.identification = id;
    }
    public boolean IDcheckPatient(Patient p){
        if (this.identification == p.getId()){
            return true;
        }
        else{
            return false;
        }
    }
    public boolean isPatientAlready(Patient pat){
        if (this.getName().toLowerCase().compareTo(pat.getName().toLowerCase()) == 0) {
            return true;
        } else {
            return false;

        }
    }
    public String toString(){
        return "Name of Patient: " +getName() + "\nID: " + identification;
    }

}
