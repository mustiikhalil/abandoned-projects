/**
 * Created by mustafakhalil on 12/1/16.
 */
public class Doctor extends Person {
    private String speciality = "";
    private double doctorFee = 0.0;
    //private double income = 0.0;
    public Doctor() {

    }

    public Doctor(String name, String spec, double fee) {
        super(name);
        this.doctorFee = fee;
        this.speciality = spec;
    }

    public String getSpeciality() {
        return speciality;
    }
    public void setSpeciality(String spec){
        this.speciality = spec;
    }
    public double getdoctorFee() {
        return doctorFee;
    }
    public void setDoctorFee(double price){
        this.doctorFee = price;
    }


    public boolean equal(Doctor p) {
        if (this.speciality.toLowerCase().compareTo(p.getSpeciality().toLowerCase()) == 0) {
            return true;
        } else {
            return false;

        }
    }

    public String toString(){
        return "Name of doctor: " +getName() + "\nFee: " + doctorFee + "\nSpeciality: "+ speciality;
    }
    public boolean isDoctorAlready(Doctor doc){
        if (this.getName().toLowerCase().compareTo(doc.getName().toLowerCase()) == 0) {
            return true;
        } else {
            return false;

        }
    }

}