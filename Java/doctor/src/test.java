import java.util.ArrayList;

/**
 * Created by mustafakhalil on 12/9/16.
 */
public class test {
    private static double income = 0.0;
    public static ArrayList<Billing> bills = new ArrayList<>();
    public static void main(String [] args){
       Patient kevin = new Patient();
       Doctor andres = new Doctor();
       kevin.setName("Kevin");
       kevin.getId("000001");
       System.out.println(kevin.toString());
       andres.setName("Andres");
       andres.setDoctorFee(30);
       andres.setSpeciality("cardiologist");
       System.out.println(andres.toString());
       Billing bill1 = new Billing();
       bill1.setDoctor(andres);
       bill1.setPatient(kevin);
       bill1.printBill();
       Patient tracer = new Patient("Tracer","000002");
       Doctor carlos = new Doctor("Carlos", "sergent",90.0);
       Billing bill2 = new Billing(tracer,carlos);
       bill2.printBill();
       addTolist(bill1);
       addTolist(bill2);
       System.out.println("\n\nTotal Income is: " +getTotalIncome());
    }
    public static double getTotalIncome(){
        for (int i = 0; i < bills.size();i++){
            income += bills.get(i).getfee();
        }
        return income;
    }
    public static void addTolist(Billing b){
        bills.add(b);
    }
}
