
/**
 * Created by mustafakhalil on 10/26/16.
 */
public class book {

    /**
     * @nameBook the name of the book
     * @author name of the author
     * @reserved when was the book reserved
     * @reservation is the number of times the book was reserved
     * These parameters are private so no one can get access to them
     */
    private String nameBook = "";
    private String author = "";
    private int reserved = 0;
    private int reservation = 0;
    private double fee = 0.0;

    /**
     * Checks if the reserved month valid
     * between 1-12
     */

    private void reservedMonth(int reserveMonth){
        if ((0 < reserveMonth) && (reserveMonth > 12)){
            System.out.println("We don't have this month in the calender: " + reserveMonth);
        }
        else{
            this.reserved = reserveMonth;
        }
    }

    /**
     * constructor that sets them to default values
     */

    public book(){

    }

    /**
     * constructors that will take two parameters
     * @param newName set name of the book to the value entered
     * @param newAuthor set author of the book to the value entered
     */

    public book(String newName, String newAuthor){
        this.nameBook = newName;
        this.author = newAuthor;
    }

    /**
     * Creates a new book with these parameters
     * @param newName name of the book
     * @param newAuthor name of the author
     * @param newReservation how many times it was reserved
     * @param newReserved when was it reserved
     */

    public book(String newName, String newAuthor, int newReserved, int newReservation){
        this.nameBook = newName;
        this.author = newAuthor;
        this.reservation = newReservation;
        reservedMonth(newReserved);
    }

    /**
     * @return the name of the book
     */

    public String getNameBook(){
        return this.nameBook;
    }

    /**
     * @param name sets the name of the book
     */

    public void setNameBook(String name){
        this.nameBook = name;
    }

    /**
     * @return the name of the author
     */

    public String getAuthor(){
        return this.author;
    }

    /**
     * @param name sets the name of the author
     */

    public void setAuthor(String name){
        this.author = name;
    }

    /**
     * @param reserve sets the reserved time of the book
     */

    public void setReserved (int reserve){
        reservedMonth(reserve);
    }

    /**
     * @return the reserved time of the book
     */

    public int getReserved (){
        return this.reserved;
    }

    /**
     * @param reserve it sets how many times did the book get reserved
     */

    public void setReservation (int reserve){
        this.reservation = reserve;
    }

    /**
     * @return it returns the times the book got reserved
     */

    public int getReservation (){
        return this.reservation;
    }

    /**
     * @return Returns a string having all the information of the book.
     */

    public String toString(){
        return "Name: " + nameBook + "\nAuthor: " + author + "\nReserved Month: "
                + reserved + "\nReservation Amount: " + reservation ;
    }

    /**
     * this method calculates the fee the person has to pay for returning the book late
     * @param currentMonth gets the current month
     * @return returns the fee that the person has
     */

    public double getFee(int currentMonth){
        double feeAmount = 0.0;
        if ((reserved == currentMonth)||(reserved-1 ==currentMonth)){
            feeAmount = 0.0;
        }
        else{
            feeAmount = (currentMonth-reserved)*0.5;
        }
        return feeAmount;
    }

    /**
     * @param findPopular get's the book that system will compare it to the current one, and see which
     *                    one has better reservations "which one is more popular"
     */

    public void findPopular(book findPopular){
        if (reservation > findPopular.getReservation()){
            System.out.println(nameBook + " is more popular than " + findPopular.getNameBook());
        }
        else{
            System.out.println( findPopular.getNameBook() + " is more popular than " +nameBook);
        }
    }

}
