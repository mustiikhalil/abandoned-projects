import java.util.Scanner;

/**
 * Created by mustafakhalil on 10/27/16.
 */
public class BookTest {

    /**
     * @month this is the default value of the current month
     */
    private static int currentMonth = 11;

    public static void main(String [] args) {
        book theRaven = new book();
        theRaven.setAuthor("Edgar Allan Poe");
        theRaven.setNameBook("The Raven");
        theRaven.setReserved(9);
        theRaven.setReservation(21);

        book christmasCarol = new book("Christmas Carol", "Charles Dickens");
        christmasCarol.setReservation(28);
        christmasCarol.setReserved(1);

        book hobbit = new book("hobbit", "J.R.R Tolkien", 3, 35);

        System.out.println(" show books in store ");
        printBooks(hobbit);
        System.out.println();
        printBooks(theRaven);
        System.out.println();
        printBooks(christmasCarol);
        System.out.println();

        book newBook = addnewBook();
        printBooks(newBook);

        System.out.println();
        System.out.println(" List of fees for every stored book ");
        fee(hobbit);
        System.out.println();
        fee(theRaven);
        System.out.println();
        fee(christmasCarol);
        System.out.println();
        fee(newBook);
        System.out.println();
        System.out.println(" Find the more popular book  ");
        newBook.findPopular(hobbit);
        newBook.findPopular(theRaven);
        newBook.findPopular(christmasCarol);

    }

    /**
     * @param toBePrinted gets the book that needs to be printed and sends it to a @toString method created in the
     *                    book class to print it
     */

    public static void printBooks(book toBePrinted){
        System.out.println(toBePrinted.toString());
    }

    /**
     * @param toBePrinted the book that the fee for it will be calculated
     *  Prints the name of the book with the amount of fee and the months with it
     */

    public static void fee(book toBePrinted){
        System.out.println("Name Book: "+toBePrinted.getNameBook() +"\nAuthor is: "+ toBePrinted.getAuthor()
                +"\nReserved Month: "+toBePrinted.getReserved()+" --- Current month: "
                + currentMonth +" --- Fee is: " + toBePrinted.getFee(currentMonth));

        //current month is a private value at the beginning of the class that has the current month saved in it
    }

    /**
     * @return  returns a newly added book to the library.
     *          that was created by the user
     */

    public static book addnewBook(){
        Scanner input = new Scanner(System.in);
        book newBook = new book();
        System.out.println("Add a new reserved book");
        System.out.print("Name of the book: ");
        newBook.setNameBook(input.nextLine());
        System.out.print("Name of the Author: ");
        newBook.setAuthor(input.nextLine());
        System.out.print("Reserved Month (1-12): ");
        newBook.setReserved(input.nextInt());
        System.out.print("Reservation Amount: ");
        newBook.setReservation(input.nextInt());
        return newBook;
    }

}
