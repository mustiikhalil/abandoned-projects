import Foundation
class bankaccounts: BankNames{
    fileprivate var accounts = [String: account]()
    override init() {
        super.init()
    }
    func addaccount(_ accountInfo: account){
        accounts[accountInfo.getAccountNumber()] = accountInfo
        placeBankName(accountInfo.getName(), Code: accountInfo.getCode())
    }
    func getaccount(_ accountNumber: Int){
        var error: Int = 1
        for (key,accs) in accounts{
            if Int(key)! == accountNumber{
                accs.printValue()
                error = 0
                break;
            }
            error = 1
        }
        if (error == 1){
            print("account not found! ")
            exit(2)
        }
        
    }
    
    func returnForTran(_ accountnumber: Int) -> account{
        for (key,accs) in accounts{
            if Int(key)! == accountnumber{
                return accs
            }
        }
        exit(9)
    }
    func logIn(_ accNumber: String, Pass: String){
        var found = 0
        for (key,accs) in accounts{
            if Int(key)! == Int(accNumber)!{
                accs.LogIn(self.getBankName(), accNum: accNumber, accPass: Pass)
                found = 1
            }
        }
        if found == 0{
            print("not found")
            exit(1231)
        }
        
    }
    func deposit(_ name: String, amount: Double) {
        for (keys, acc) in accounts{
            if Int(keys)! == Int(name)!{
                acc.Deposit(amount)
            }
        }
    }
    func withdraw(_ name: String, amount: Double) {
        for (keys, acc) in accounts{
            if Int(keys)! == Int(name)!{
                acc.withDraw(amount)
            }
        }
    }
    
    func transfer(_ myAccount: Int, otherAccount: Int, amount: Double, otherBank: bankaccounts){
        for (key,accs) in accounts{
            if Int(key)! == myAccount{
                if accs.loggedIn() == true{
                let number = accs.withDrawreturn(amount)
                    for (key,acco) in otherBank.accounts{
                        if Int(key)! == otherAccount{
                            acco.Deposit(number)
                            break;
                        }
                    }
                }
            }
        }
    }
    func checkIfExists(_ accNumber: String, name: String, surname: String) -> Bool{
        for (accountnumber ,accountHolder) in accounts{
            if Int(accountnumber)! == Int(accNumber)!{
                print("can't open with an existing account number")
                return false
            }
            if accountHolder.getUserName().lowercased() == name.lowercased(){
                if accountHolder.getSURName().lowercased() == surname.lowercased(){
                    print("can't open two bank accounts at the same place")
                    return false
                }
                else{
                    continue
                }
            }
        }
        return true
    }
    
}
