import Foundation

class BankNames{
    fileprivate var bankName: String = ""
    fileprivate var BankCode: Int = 0
    init(){
        
    }
    func placeBankName(_ name: String, Code: Int) {
        self.BankCode = Code
        self.bankName = name
    }
    func getBankName() -> String{
        return bankName
    }
    func getBankCode() -> Int{
        return BankCode
    }
}
