
import AppKit
import Foundation

class account: Bank, accountLibrary{
    fileprivate var accountNumber: String = "000000"
    fileprivate var accountPassword: String = "0000"
    fileprivate var accBalance: Double = 0.0
    fileprivate var LoggedIn: Bool = false
    fileprivate var encodingPass = 0
    internal var access: String = ""
    fileprivate var surname: String = ""
    func accessable(name: String){
        if (self.access == name){
            print("Can be accessed")
        }
        else{
            print("Cant be accessed")
        }
    }
    func getAccountNumber() -> String{
        return self.accountNumber
    }
    func status() -> Bool{
        return LoggedIn
    }
    
    init(nameUser: String, sirname: String,nameBank: String, accNum: String, accPass: String, accBal: Double){
        super.init(name: nameBank)
        self.access = nameUser
        self.surname = sirname
        if (accNum.characters.count > 5){
            self.accountNumber = accNum
        }
        else{
            print("Low account number")
            //accountProblem.insufficientlenght
            exit(1)
        }
        if (accPass.characters.count > 3){
            codePass(accPass)
        }
        else{
            print("Low password number")
            //accountProblem.invalidPassword
            exit(1)
        }
        if (accBal >= 0){
            self.accBalance = accBal
        }
        else{
            print("less than zero!")
            //accountProblem.lessthanZero
            exit(1)
        }
    }
    
    func LogIn(_ nameBank: String, accNum: String, accPass: String){
        if true == checkBank(name: nameBank.lowercased){
            if (accNum == self.accountNumber){
                if (self.decodePass(accPass)==true){
                    print("Access Granted!")
                }
            }
            else{
                print("Not in DataBase!")
                exit(-1)
            }
        }
        else{
            print("Wrong bank")
            exit(11)
        }
    }
    
    override func printValue(){
        super.printValue()
        print(self.accountNumber)
        print(self.access)
        print(self.accBalance)
    }
    
    fileprivate func codePass(_ accPass: String){
        let codingPass = Int(arc4random_uniform(10))
        var newPass: String = ""
        for numb in accPass.characters{
            let number = Int(String(numb))
            let codedNum = (number! + codingPass)
            let changedNum = unichar(codedNum)
            //let char = Character(changedNum)
            newPass += String(changedNum)
        }
        self.encodingPass = codingPass
        self.accountPassword = newPass
    }
    
    fileprivate func decodePass(_ accPass: String) -> Bool{
        var newPass: String = ""
        for numb in accPass.characters{
            let number = Int(String(numb))
            let codedNum = (number! + encodingPass)
            let changedNum = unichar(codedNum)
            newPass += String(changedNum)
        }
        if (newPass == self.accountPassword){
            self.LoggedIn = true
            return true
        }
        else{
            print("IncorrectPassword")
            //accountProblem.incorrectPassword
            exit(-1)
        }
    }
    
    func Deposit(_ depAmount: Double){
        if (depAmount > 0.0){
            self.accBalance = self.accBalance + depAmount
            print("\(depAmount) is deposited into your account \(self.accountNumber)  your new balance is \(self.accBalance)")
        }
        else{
            print("Can't deposit Negative amount")
            exit(123)
        }
    }
    
    func withDraw(_ amount: Double){
        if (amount > 0.0){
            if (amount < self.accBalance){
                self.accBalance = self.accBalance - amount
                print("\(amount) is withdrawn into your account \(self.accountNumber) your new balance is \(self.accBalance)")
            }
            else{
                print("No enough money!")
                exit(123)
            }
        }
        else{
            print("Can't withdraw!")
            exit(123)
        }
    }
    func loggedIn() -> Bool{
        return LoggedIn
    }
    func getUserName() -> String {
       return access
    }
    func getSURName() -> String {
        return surname
    }
    
    func withDrawreturn(_ amount: Double) -> Double{
        if (amount > 0.0){
            if (amount < self.accBalance){
                self.accBalance = self.accBalance - amount
                print("\(amount) is withdrawn into your account \(self.accountNumber) your new balance is \(self.accBalance)")
            }
            else{
                print("No enough money!")
                exit(1212)
            }
        }
        else{
            print("Can't withdraw!")
            exit(1213)
        }
        return amount
    }

    
    fileprivate enum accountProblem: Error {
        case invalidPassword
        case insufficientlenght
        case lessthanZero
        case incorrectPassword
        case cantbefound
    }
}

