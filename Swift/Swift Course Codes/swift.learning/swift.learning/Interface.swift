import Foundation
class Interface{
    var banks = [bankaccounts()]
    var accountBanking = bankaccounts()
    var accountnumber = ""
    var pass = ""
    init(bank: bankaccounts, bankslist: [bankaccounts]){
        self.banks = bankslist
        self.accountBanking = bank
        welcomeMenu()
        Page()
    }
    func welcomeMenu() {
        print("Welcome to our ATM!")
        print("enter your account number and password: ")
        self.accountnumber = readLine(strippingNewline: true)!
        self.pass = readLine(strippingNewline: true)!
        self.accountBanking.logIn(accountnumber, Pass: pass)
    }
    func forwardMoney(){
        print("Enter the amount you wanna transfer: ")
        let amount = Double(readLine(strippingNewline: true)!)!
        print("Enter bank name: ")
        let nameOfBank = readLine(strippingNewline: true)!
        print("account number: ")
        let accountNum = Int(readLine(strippingNewline: true)!)!
        for bank in banks{
            if nameOfBank == bank.getBankName(){
                accountBanking.transfer(Int(accountnumber)!, otherAccount: accountNum, amount: amount, otherBank: bank)
            }
        }
    }
    func Page(){
        print("to deposit press 1")
        print("to withdraw press 2")
        print("to transfer press 3")
        print("to exit press 4")
        let command = readLine(strippingNewline: true)!
        switch command {
        case "1":
            print("amount wanted to deposit")
            let amount = readLine(strippingNewline: true)!
            accountBanking.deposit(self.accountnumber, amount: Double(amount)!)
            print("want another transaction? ")
            let transaction = Int(readLine(strippingNewline: true)!)!
            if 1 == transaction{
                Page()
            }
        case "2":
            print("amount wanted to withdraw")
            let amount = readLine(strippingNewline: true)!
            accountBanking.withdraw(self.accountnumber, amount: Double(amount)!)
            print("want another transaction? ")
            let transaction = Int(readLine(strippingNewline: true)!)!
            if 1 == transaction{
                Page()
            }
        case "3":
            forwardMoney()
            print("want another transaction? ")
            let transaction = Int(readLine(strippingNewline: true)!)!
            if 1 == transaction{
                Page()
            }
        case "4":
            exit(1)
        default:
            exit(0)
        }
        
    }
}
