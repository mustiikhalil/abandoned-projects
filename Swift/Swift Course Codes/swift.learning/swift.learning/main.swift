
import AppKit
import Foundation

//
//  main.swift
//  swift.learning
//
//  Created by Mustafa Khalil on 4/10/16.
//  Copyright © 2016 Mustafa Khalil. All rights reserved.
//
/*
 var welcomeScreen: String
 func input() -> String{
 let keyboard = NSFileHandle.fileHandleWithStandardInput()
 let inputdata = keyboard.availableData
 return NSString(data: inputdata, encoding: NSUTF8StringEncoding) as! String
 }
 
 print("PLease enter a message: ")
 var inputuser = input()
 welcomeScreen = inputuser
 print(welcomeScreen)
 */

//
//  main.swift
//  trialString
//
//  Created by Suha Baobaid on 21/04/16.
//  Copyright © 2016 Suha Baobaid. All rights reserved.
//

/*
 
 import Foundation
 let π = 3.14159
 var  friendlymessage: String
 let hello = "hello! good morning!"
 var number: Int = 0
 let  individualScores = [75, 43, 103, 87, 12]
 var  teamScore = 0
 var red, green, blue: Double
 
 var x = 12e2
 print(x)
 var y = 0
 var z = 0
 
 number = 3
 
 friendlymessage = "bonjour" // good morning in french!
 var i = 0
 var j = 0
 print("\(hello) how are you? ")
 
 
 print("Hello, World!")
 
 var welcomeScreen: String = "hello world!"
 print("Please enter a number: ")
 let inputuser = readLine(stripNewline: true)
 
 var number = Int(inputuser!)
 
 if number == 20{
 print("welcome home! this is 20")
 }
 else{
 print("Get out of here!!!")
 if let inputted = inputuser{
 print("\(inputted).. is a ")
 }
 }
 let message = (statusCode: 404,statusMessage: "not found")
 print("error \(message.statusCode) the page is \(message.statusMessage)")
 
 let test = (100,"hi")
 let (testnum, mess) = test
 var result = testnum + 10
 print(result)

 
 
 var number: Int? = 102
 print(number)
 number = nil
 if number == nil{
 print("This number has no value")
 }
 number = 101
 
 print(number)
 
 var test = "101"
 if let actualNumber  = Int(test){
 var printable = actualNumber
 print("it has a int value of: \(printable)")
 if 101 == actualNumber{
 print("they are equal")
 }
 }
 else{
 print("It's not an Int")
 }
 if let firstNumber = Int("4"), secondNumber = Int("42") where firstNumber < secondNumber{
 print("\(firstNumber) first < \(secondNumber)second")
 }
 var total = 0
 
 let coord: [[Int]] = [[0,01,02,03],
 [10,11,12,13],
 [20,21,22,23]]
 for colums in coord{
 for rows in colums{
 print(rows , colums)
 }
 }
 
 
 for var i: Int = 1; i <= 10; i += 1 {
 if (i%2 == 1){
 continue
 }
 else if (i == 10){
 break
 }
 else{
 print(i)
 }
 }

 var numberOfA = 0
 var quote = "I wanna live my life without any pressure"
 quote = quote.lowercaseString
 print(quote)
 for singleChar in quote.characters{
 if singleChar == "a"{
 numberOfA += 1
 }
 }
 print(numberOfA)

 let people: Array<String> = ["bob", "Mustafa", "Khalil"]
 
 for peeps in people{
 if peeps == "Mustafa"{
 print("done")
 }
 }
 
 
 let heroes = [1 : "superman", 2: "Batman", 3: "wonderwomen"]
 
 for (key,name) in heroes{
 print("\(key) is the number of \(name)")
 }
 
 var k = 1
 
 while k < 10{
 print(k)
 k += 1
 }
 
 k = 10
 repeat{
 print(k)
 k-=1
 }while k != 0
 
 
 func add(num1: Int, num2: Int) -> Int{
 return num1 + num2
 }
 print(add(3, num2: 5))
 
 func addlist(Num: Int...) -> Int{
 var sum = 0
 for num in Num{
 sum = sum + num
 }
 return sum
 }
 print(addlist(1,2,3,4,5))
 
 var string1 = "sad"
 var string2 = "happy"
 func makeUpper(inout Str1: String,inout Str2: String){
 Str1 = Str1.uppercaseString
 Str2 = Str2.uppercaseString
 }
 makeUpper(&string1, Str2: &string2)
 
 print(string1, string2)

 
 
 func getMulti(number: Int) -> (x2: Int, x3: Int){
 var x2 = number*2
 let x3 = number*3
 return (x2,x3)
 }
 
 var answer = getMulti(5)
 print(answer.x2)
 print(answer.x3)
 answer.x2 = 12
 print(answer.x2)
 
 
 func average(nums: Int...) -> Double{
 var sum = 0
 for num in nums{
 sum = sum+num
 }
 return Double(sum)/Double(nums.count)
 }
 func add(nums: Int...) -> Double {
 var sum = 0
 for num in nums{
 sum = sum+num
 }
 return Double(sum)
 }
 func doMath(mathOption: String) -> (Int...) -> Double{
 if mathOption == "average"{
 return average
 }else{
 return add
 }
 }
 
 var math = "add"
 var math0ption = doMath(math)
 print(math0ption(1,2,3,4))
 
 var square: (Int) -> (Int) = { num in
 return num*num
 }
 
 print(square(5))
 
 
 
 struct Runner {
 var name: String
 var milePace: Double
 static let marthonDis = 26.2
 func displayMP() -> (seconds: String,adspace: String) {
 let adspace = Int(self.milePace)
 let prctmin = self.milePace - Double(adspace)
 let seconds = prctmin * 60
 return ("adspace: \(adspace)","seconds: \(seconds)")
 }
 
 var marthonTime : Double{
 get{
 return (self.milePace * 26.2)/60
 }
 }
 }
 let bolt = Runner(name: "Bolt", milePace: 4.68)
 let kayle = Runner(name: "kayle", milePace: 4.7)
 
 print("\(bolt.name) runs at \(bolt.displayMP()) finishes the marthon at time of \(bolt.marthonTime)")
 print("\(kayle.name) runs at \(kayle.displayMP()) finishes the marthon at time of \(kayle.marthonTime)")
 print(Runner.marthonDis)
 
 
 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!   CLASSES  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 
 class animal{
 var name: String = "No name"
 var weight: Double = 0.0
 init(name: String, Weight: Double){
 self.name = name
 self.weight = Weight
 }
 func DisplayIt() {
 print("\(self.name) has the weight of \(self.weight)")
 }
 }
 
 
 var cat = animal(name: "kitty", Weight: 2.1)
 cat.DisplayIt()
 
 var garanti = Bank(name: "garanti")
 
 var Mustafa = account(nameBank: "is bank",accNum: "200100", accPass: "1234", accBal: 3.0)
 
 garanti.printvalue()
 Mustafa.printvalue()
 Mustafa.logIn("00000", accPass: "4182")

 
 print("is garanti a bank \(Mustafa is account)")
 
 
 
 func printval(bank: Bank){
 bank.printvalue()
 }
 
 //printval(Mustafa)
 //printval(garanti)
 
 
 */

var Mustafa = account(nameUser: "Mustafa",sirname: "Khalil", nameBank: "garanti", accNum: "354717", accPass: "6742", accBal: 1900)
var nader = account(nameUser: "nader",sirname: "Jaber", nameBank: "garanti", accNum: "354718", accPass: "7281", accBal: 4000)
var ibrahim = account(nameUser: "ibrahim",sirname: "Hajar", nameBank: "garanti", accNum: "354719", accPass: "1212", accBal: 4000)
var leyla = account(nameUser: "leyla",sirname: "Eid", nameBank: "vakif", accNum: "891821", accPass: "3000", accBal: 4000)
var ahmed = account(nameUser: "ahmed",sirname: "Jaber", nameBank: "DENIZ", accNum: "333210", accPass: "9281", accBal: 5321)
var garanti = bankaccounts()
var isbank = bankaccounts()
var vakif = bankaccounts()
var hsbc = bankaccounts()
var deniz = bankaccounts()
var banks = [bankaccounts()]
banks.append(garanti)
banks.append(isbank)
banks.append(vakif)
banks.append(hsbc)
banks.append(deniz)

garanti.addaccount(Mustafa)
garanti.addaccount(nader)
garanti.addaccount(ibrahim)
deniz.addaccount(ahmed)
vakif.addaccount(leyla)

OpeningBankAccount(bankslist: banks)


//Interface(bank: garanti, bankslist: banks)

//vakif.getaccount(891821)
//print()
//garanti.getaccount(354717)
//garanti.logIn("354717", Pass: "6742")
//garanti.transfer(354717, otherAccount: 891821, amount: 1000, otherBank: vakif)
//print()
//vakif.getaccount(891821)
//print()
//garanti.getaccount(354717)

